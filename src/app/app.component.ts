import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private http: HttpClient,
    private formBuilder: FormBuilder) { }
  title = 'innotech';
  fileDataFund: File;
  fileDataBay: File;
  check_fund_to_bay = false;
  check_bay_to_connext = false;
  textOnFund = '';
  changedTypefileBay = '';
  changedTypefileFund = '';
  fileDataFundname;
  fileDataBayname;
  alertPopup;
  checkAlertUpload = false;
  dd_fund_to_bay = ['New Acount','Approved Transaction'];
  dd_fund_to_fund = ['Allotted Transaction', 'Unitholder Balance','NAV'];

  api_url = 'http://localhost:8080/api/v1';
  uploadForm: FormGroup;
  uploadFormBay: FormGroup;

  config = {
    displayKey: 'display_name',
    search: true,
    height: 'auto',
    placeholder: 'Select File To Convert',
    customComparator: () => { },
    limitTo: 10,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'display_name',
  };
  
  selectionChangedFund(event) {
  	this.changedTypefileFund = event['value'];
  }

  selectionChangedBay(event) {
    this.changedTypefileBay = event['value'];
  }

  checkFundconnext() {
  	this.check_fund_to_bay = true;
  	this.check_bay_to_connext = false;
  	this.textOnFund = 'FundConnext To Bay';
  }

  checkBayConnext() {
  	this.check_fund_to_bay = false;
  	this.check_bay_to_connext = true;
  	this.textOnFund = 'Bay To FundConnext';
  }

  fileProgressFund(event: any) {
    this.fileDataFund = <File>event.target.files[0];
    this.uploadForm.get('profile').setValue(this.fileDataFund);
    // console.log(this.fileDataFund)
    this.fileDataFundname = <File>event.target.files[0]['name'];
  }

  fileProgressBay(event: any) {
    this.fileDataBay = <File>event.target.files[0];
    this.uploadFormBay.get('profile1').setValue(this.fileDataBay);
    this.fileDataBayname = <File>event.target.files[0]['name'];
  }

  uploadFiletoBay() {
    if (this.changedTypefileFund == 'New Acount') {

        const formData = new FormData();
        formData.append('file', this.uploadForm.get('profile').value);
       
      this.http.post(this.api_url + '/account',formData).subscribe(data => { 
        this.alertPopup = data['message'];
        this.checkAlertUpload = true;
        if (this.checkAlertUpload) {
          setTimeout(() => {
            this.checkAlertUpload = false;
          }, 3000);
        }
      }, error =>{
        console.log(error)
      });
    } else if (this.changedTypefileFund == 'Approved Transaction') {
          const formData = new FormData();
          formData.append('file', this.uploadForm.get('profile').value);

        this.http.post(this.api_url + '/approved',formData).subscribe(data => { 
          this.alertPopup = data['message'];
          this.checkAlertUpload = true;
          if (this.checkAlertUpload) {
            setTimeout(() => {
              this.checkAlertUpload = false;
            }, 3000);
          }
        }, error =>{
          console.log(error)
        });
    }
  }

  uploadFiletoFund() {
    if (this.changedTypefileBay == 'Allotted Transaction') {
          const formData = new FormData();
          formData.append('file', this.uploadFormBay.get('profile1').value);

        this.http.post(this.api_url + '/alloted',formData).subscribe(data => { 
          this.alertPopup = data['message'];
          this.checkAlertUpload = true;
          if (this.checkAlertUpload) {
            setTimeout(() => {
              this.checkAlertUpload = false;
            }, 3000);
        }

        }, error =>{
          console.log(error)
        });
    } else if (this.changedTypefileBay == 'Unitholder Balance') {
        const formData = new FormData();
        formData.append('file', this.uploadFormBay.get('profile1').value);

        this.http.post(this.api_url + '/unitholder',formData).subscribe(data => { 
          this.alertPopup = data['message'];
          this.checkAlertUpload = true;
          if (this.checkAlertUpload) {
            setTimeout(() => {
              this.checkAlertUpload = false;
            }, 3000);
          }
        }, error =>{
          console.log(error)
        });
    } else if (this.changedTypefileBay == 'NAV') {
        const formData = new FormData();
        formData.append('file', this.uploadFormBay.get('profile1').value);

        this.http.post(this.api_url + '/nav',formData).subscribe(data => { 
          this.alertPopup = data['message'];
          this.checkAlertUpload = true;
          if (this.checkAlertUpload) {
            setTimeout(() => {
              this.checkAlertUpload = false;
            }, 3000);
          }
        }, error =>{
          console.log(error)
        });
    }
  }
  ngOnInit() {
    this.dd_fund_to_bay;
    this.dd_fund_to_fund;
    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });
    this.uploadFormBay = this.formBuilder.group({
      profile1: ['']
    });
  }

}







